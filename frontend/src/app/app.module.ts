import { NgModule } from '@angular/core';

import { BrowserModule, Title } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { SkillsComponent } from './skills/skills.component';
import { HardSkillDetailComponent } from './hardSkillDetail/hardSkillDetail.component';
import { EmployeeService } from './shared/services/employee.service';
import { ApiService } from './shared';
import { routing } from './app.routing';
import { SearchBasicComponent } from './searchEngine/searchBasic/searchBasic.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ProjectService } from './shared/services/project.service';
import { Http } from '@angular/http';
import { AppService } from './app.service';

import { AdvancedSearchComponent } from './searchEngine/advancedSearch/advancedSearch.component';
import { ConnectionBackend } from '@angular/http';
import { XHRBackend } from '@angular/http';




@NgModule({
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    routing,
    NgbModule.forRoot()
  ],
  declarations: [
    AppComponent,
    HomeComponent,
    SkillsComponent,
    HardSkillDetailComponent,
    SearchBasicComponent,
    AdvancedSearchComponent
  ],
  providers: [
    Title,
    ApiService,
    EmployeeService,
    ProjectService,
    { provide: ConnectionBackend, useClass: XHRBackend },

    { provide: Http, useClass: AppService }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {

}
