import { Injectable } from '@angular/core';
import { ConnectionBackend, RequestOptions, RequestOptionsArgs, Response, Http, XHRBackend } from '@angular/http';
import { Observable } from 'rxjs/Rx';
/** Code Error  HTTP 401 Unauthorized */
export const SC_UNAUTHORIZED = 401;


/**
 * Global Service Http
 */
@Injectable()
export class AppService extends Http {
    /** main path backend  */
    protected path: string;

    /**
     * Extract response and  convert  to json
     * @param response
     */
    public static extractData(response: Response): Response | Object {
        const result: Response = response.json();
        return result || {};
    }

    /**
     * Error admin
     * @param error
     */
    public static handleError(error: Response): Observable<Object> {
        let errorMessage: string;
        if (error instanceof Response) {
            errorMessage = AppService.getResponseError(error);
            if (error.status === SC_UNAUTHORIZED) {
                console.error(Response);
                return;
            }
        } else {
            errorMessage = AppService.getGenericError(error);
        }
        throw error;
    }

    /**
     * generate error reponse
     * @param error
     */
    private static getResponseError(error: Response): string {
        let errorMessage: string;
        errorMessage = `${error.status} - ${error.statusText || ''}`;
        return errorMessage;
    }

    /**
     * generic error convet to string
     * @param error
     */
    private static getGenericError(error: any): string {
        let errorMessage: string;
        errorMessage = error.message ? error.message : error.toString();
        return errorMessage;
    }

    /**
     * Constructor.
     * @param backend  service backend
     * @param defaultOptions  default options for   get the service
     */
    constructor(backend: ConnectionBackend, defaultOptions: RequestOptions) {
        super(backend, defaultOptions);
        this.path = 'http://localhost:3000';
    }

    /**
     * Execute request get
     * @param url
     * @param options
     */
    public get(url: string, options?: RequestOptionsArgs): Observable<Response | any> {
        url = this.updateUrl(url);
        return super.get(url, options)
            .map(AppService.extractData)
            .catch(AppService.handleError);
    }

    /**
     * Execute  request post
     * @param url
     * @param body
     * @param options
     */
    public post(url: string, body: string, options?: RequestOptionsArgs): Observable<Response | any> {
        url = this.updateUrl(url);
        return super.post(url, body, options)
            .map(AppService.extractData)
            .catch(AppService.handleError);
    }

    /**
     * Execute request put
     * @param url
     * @param body
     * @param options
     */
    public put(url: string, body: string, options?: RequestOptionsArgs): Observable<Response | any> {
        url = this.updateUrl(url);
        return super.put(url, body, options)
            .map(AppService.extractData)
            .catch(AppService.handleError);
    }

    /**
     * Execute request delete
     * @param url
     * @param options
     */
    public delete(url: string, options?: RequestOptionsArgs): Observable<Response | any> {
        url = this.updateUrl(url);
        return super.delete(url, options)
            .map(AppService.extractData)
            .catch(AppService.handleError);
    }

    /**
     *  Update  path for get  service  backend
     * @param req
     */
    private updateUrl(req: string): string {
        return this.path + req;
    }
}

/**
 * Return new instance for AppServices
 * @param xhrBackend
 * @param requestOptions
 */
export function getAppServiceFactory(xhrBackend: XHRBackend, requestOptions: RequestOptions): Http {
    return new AppService(xhrBackend, requestOptions);
}

