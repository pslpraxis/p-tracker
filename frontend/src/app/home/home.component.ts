import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../shared';
import { Title }     from '@angular/platform-browser';


@Component({
  selector: 'my-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  // Used to store all values returned by the api
  protected projects: Array<any>;
  protected message: string = 'El usuario no ha participado en ningun proyecto'; 
  
  

  // Recives one object api to ask for data to the backend
  constructor(private titleService: Title, protected api: EmployeeService) {
    api.getProjects().subscribe((projects) => {
      this.projects = projects;
    });

    
  }

  ngOnInit() {
    this.titleService.setTitle('Home');
  }

}


