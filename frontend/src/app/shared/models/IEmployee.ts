
export interface IEmployee {
    name: string;
    cellPhone: number;
    email: string;
    workPlace: string;
    EmploymentContract: number;
    projects: Array<any>;
    userAccount: string;
    emailPSL: string;
    skypeAccount: string;
    userPermission: string;
}
