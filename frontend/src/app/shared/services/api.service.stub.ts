import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { employees } from './api.fixture';
@Injectable()
export class ApiServiceStub {

    // Resolve HTTP using the constructor
    constructor() { }

    // Fetch all existing comments
    public getEmployees(): Observable<Response & any> {
        // ...using get request
        return Observable.of(employees);
    }
}
