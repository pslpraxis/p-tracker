import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { projects } from './api.fixture';


@Injectable()
export class ProjectServiceStub {

    // Resolve HTTP using the constructor
    constructor() { }

    // Fetch all existing comments
    public getProjects(): Observable<Response & any> {
        // ...using get request
        return Observable.of(projects);
    }

    public getProjectsHardskills(): Observable<Response & any> {
        /*db.projects.find({"employees.hard_skills.skill_name":"Git"},{"project_name":1})  */
        return Observable.of([]);
    }

};
