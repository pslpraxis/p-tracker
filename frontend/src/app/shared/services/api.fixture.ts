import { IEmployee } from '../models/IEmployee';

export const employees: IEmployee[] = [
    {
        email: 'abc@algo.com',
        cellPhone: 111,
        emailPSL: 'abc@psl.com.co',
        EmploymentContract: 9,
        name: 'name',
        projects: [],
        skypeAccount: 'userSkype',
        userAccount: '',
        userPermission: '',
        workPlace: ''
    },
    {
        email: 'abc@algo.com',
        cellPhone: 111,
        emailPSL: 'abc@psl.com.co',
        EmploymentContract: 9,
        name: 'name',
        projects: [],
        skypeAccount: 'userSkype',
        userAccount: '',
        userPermission: '',
        workPlace: ''
    }
];


export const projects: any[] = [
   {
       name: 2,
       id: 2
   }
];
