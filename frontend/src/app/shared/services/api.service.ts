import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

@Injectable()
export class ApiService {

  // Resolve HTTP using the constructor
  constructor(private http: Http) { }

  // Fetch all existing comments
  public getEmployees(): Observable<Response & any> {

    // ...using get request
    return this.http.get('/employees');


  }
}
