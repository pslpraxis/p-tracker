import { Component } from '@angular/core';

import { ProjectService } from '../../shared';
import { IEmployee } from '../../shared';
import { ApiService } from '../../shared';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/map';

/*function getJson() {
  let json = [
    {'name': 'Alabama', 'flag': '5/5c/Flag_of_Alabama.svg/45px-Flag_of_Alabama.svg.png'},
    {'name': 'Alaska', 'flag': 'e/e6/Flag_of_Alaska.svg/43px-Flag_of_Alaska.svg.png'},
    {'name': 'Arizona', 'flag': '9/9d/Flag_of_Arizona.svg/45px-Flag_of_Arizona.svg.png'},
    {'name': 'Arkansas', 'flag': '9/9d/Flag_of_Arkansas.svg/45px-Flag_of_Arkansas.svg.png'},
    {'name': 'Florida', 'flag': 'f/f7/Flag_of_Florida.svg/45px-Flag_of_Florida.svg.png'},
    {
      'name': 'Georgia',
      'flag': '5/54/Flag_of_Georgia_%28U.S._state%29.svg/46px-Flag_of_Georgia_%28U.S._state%29.svg.png'
    },
    {'name': 'Wyoming', 'flag': 'b/bc/Flag_of_Wyoming.svg/43px-Flag_of_Wyoming.svg.png'}
  ];
  return json;
}
*/
/**
 * Component is used for the view  of search basic by employees and projects
 */
@Component({
    templateUrl: './advancedSearch.component.html',
    styleUrls: ['./advancedSearch.component.scss'],
    selector: 'my-advanced-search'
})

export class AdvancedSearchComponent {

  /** employees list  */
  protected employees: Array<IEmployee>;
  /** projects  */
  protected projects: Array<any>;
  /** search basic  */
  protected advancedSearch: string;
  /** result list */
  protected resultList: Array<any> = [];
  /** filter  */
  protected filter: any;

  constructor(protected api: ApiService, protected projectService: ProjectService) {
      this.projectService.getProjectsHardskills().subscribe((projects) => {
          this.projects = projects;
      });
      
      this.filter = '{"list": "projects", "attr": "project_name"}';
  }

    search = (text$: Observable<string>) =>
    text$
      .debounceTime(200)
      .map(term => term === '' ? []
        : this.projects.filter(v => v.project_name.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10));

    formatter = (x: {project_name: string}) => x.project_name;



  }
