import { TestBed } from '@angular/core/testing';

import { SearchBasicComponent } from './searchBasic.component';
import { AppModule } from '../../app.module';
import { RouterTestingModule } from '@angular/router/testing';
import { ApiService } from '../../shared/services/api.service';
import { ApiServiceStub } from '../../shared/services/api.service.stub';
import { ProjectService } from '../../shared/services/project.service';
import { ProjectServiceStub } from '../../shared/services/project.service.stub';
import { employees, projects } from '../../shared/services/api.fixture';




describe('SeacrchBasicComponent', () => {
    // provide our implementations or mocks to the dependency injector
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [AppModule, RouterTestingModule],
            providers: [
                { provide: ApiService, useClass: ApiServiceStub },
                { provide: ProjectService, useClass: ProjectServiceStub }
            ]
        });
    });

    it('instance the componente succesfull and load data correct', () => {
        let fixture = TestBed.createComponent(SearchBasicComponent);
        expect(fixture.componentInstance instanceof SearchBasicComponent).toBe(true);
        expect(fixture.componentInstance.employees).toBe(employees);
        expect(fixture.componentInstance.projects).toBe(projects);
    });


});
