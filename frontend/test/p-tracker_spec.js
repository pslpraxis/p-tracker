//Control the speed test execution
var origFn = browser.driver.controlFlow().execute;
browser.driver.controlFlow().execute = function () {
	var args = arguments;
	// queue 100ms wait
	origFn.call(browser.driver.controlFlow(), function () {
		return protractor.promise.delayed(80);   // here we can adjust the execution speed
	});
	return origFn.apply(browser.driver.controlFlow(), args);
};


describe("connection p-tracker site",function(){
	it("to test connection", function(){
		 browser.get('http://localhost:4000');
	})	
});


describe("basic search test",function(){
	it("to test connection", function(){
		browser.get('http://localhost:4000/search-basic');
		element(by.css('.custom-control.custom-radio.em')).click();
		element(by.css('input[type="text"]')).sendKeys('Jose Narvaez');;
		browser.actions().sendKeys(protractor.Key.ENTER).perform();

		
		var tabledata =element.all(by.css("table"));	//Get the values in the resulting table		
		var rows =tabledata.all(by.tagName("tr"));		//Get the rows		
		var cells = rows.all(by.tagName("td"));			//Get the cells		
		//Evaluation block
		expect(cells.get(0).getText()).toEqual("Jose Narvaez");	
		expect(cells.get(1).getText()).toEqual("project1");
		expect(cells.get(2).getText()).toEqual("frontend");		
		
	})	
});

describe("try another employee",function(){
	it("to test the refresh in the page for a new search", function(){
		element(by.css('.custom-control.custom-radio.em')).click();
		element(by.css('input[type="text"]')).clear().sendKeys('Daniela Ramirez');;
		browser.actions().sendKeys(protractor.Key.ENTER).perform();

		var tabledata =element.all(by.css("table"));	//Get the values in the resulting table		
		var rows =tabledata.all(by.tagName("tr"));		//Get the rows		
		var cells = rows.all(by.tagName("td"));		

		expect(cells.get(0).getText()).toEqual("Daniela Ramirez");	
		expect(cells.get(1).getText()).toEqual("project1");
		expect(cells.get(2).getText()).toEqual("scrum");		
	})	
});

describe("try an unexisting employee",function(){
	it("to test the behavior when an employee do not appear in the database", function(){
		browser.get('http://localhost:4000/search-basic');
		element(by.css('.custom-control.custom-radio.em')).click();
		element(by.css('input[type="text"]')).clear().sendKeys('Agapito Caicedo');;
		browser.actions().sendKeys(protractor.Key.ENTER).perform();

		expect(element(by.css("table")).isPresent()).toBe(false);	
	})	
});

describe("try searching a project in the employee option",function(){
	it("to test a wrong option to search a value", function(){
		browser.get('http://localhost:4000/search-basic');
		element(by.css('.custom-control.custom-radio.em')).click();
		element(by.css('input[type="text"]')).clear().sendKeys('project4');;
		browser.actions().sendKeys(protractor.Key.ENTER).perform();

		expect(element(by.css("table")).isPresent()).toBe(false);	
	})	
});


/*To this test is needed add this employee to the database
db.employees.insert({
    "_id" : 6.0,
    "name" : "Alejandra Gonzales",
    "cellPhone" : 31225631231.0,
    "email" : "alejandragon@gmail.com",
    "workPlace" : "office12",
    "IBU-ECU" : "ni idea",
    "EmploymentContract" : 5.0,
    "projects" : [ 
        {
            "project_id" : 1.0,
            "project_name" : "project1"
        }, 
        {
            "project_id" : 4.0,
            "project_name" : "project4"
        }
    ],
    "projectNow" : {
        "project_id" : 4.0,
        "project_name" : "project4"
    },
    "userAccount" : "alejandrgupsl",
    "emailPSL" : "alejandrgona@psl.com.co",
    "skypeAccount" : "alejandrasaskype",
    "userPermission" : "employee",
    "role" : "test"
})
*/
// describe("try searching a employee who shares name with another employee",function(){
// 	it("to test a multiple answers", function(){
// 		browser.get('http://localhost:4000/search-basic');
// 		element(by.css('.custom-control.custom-radio.em')).click();
// 		element(by.css('input[type="text"]')).clear().sendKeys('Alejandra');;
// 		browser.actions().sendKeys(protractor.Key.ENTER).perform();

// 		var tabledata =element.all(by.css("table"));	//Get the values in the resulting table		
// 		var rows =tabledata.all(by.tagName("tr"));		//Get the rows		
// 		var cells = rows.all(by.tagName("td"));		

// 		expect(cells.get(0).getText()).toEqual("Alejandra Castrillon");	
// 		expect(cells.get(1).getText()).toEqual("project1");
// 		expect(cells.get(2).getText()).toEqual("scrum");	
// 		//Need captures the second row. Not working yet.	
// 		//expect(cells.get(3).getText()).toEqual("Alejandra Gonzales");	
// 		//expect(cells.get(4).getText()).toEqual("project1");
// 		//expect(cells.get(5).getText()).toEqual("test");


// 	})	
// });

describe("try searching a project",function(){
	it("to test the prokect option search by name", function(){
		browser.get('http://localhost:4000/search-basic');
		element(by.css('.custom-control.custom-radio.pronom')).click();
		element(by.css('input[type="text"]')).clear().sendKeys('project4');;
		browser.actions().sendKeys(protractor.Key.ENTER).perform();
		
		var tabledata =element.all(by.css("table"));	//Get the values in the resulting table		
		var rows =tabledata.all(by.tagName("tr"));		//Get the rows		
		var cells = rows.all(by.tagName("td"));		

		expect(cells.get(0).getText()).toEqual("project4");	
		expect(cells.get(1).getText()).toEqual("python, angular4, MongoDb");
		expect(cells.get(2).getText()).toEqual("KANBAN");
	})	
});

describe("try searching a project",function(){
	it("to test the prokect option search by technology", function(){
		browser.get('http://localhost:4000/search-basic');
		element(by.css('.custom-control.custom-radio.tecno')).click();
		element(by.css('input[type="text"]')).clear().sendKeys('python');;
		browser.actions().sendKeys(protractor.Key.ENTER).perform();
		
		var tabledata =element.all(by.css("table"));	//Get the values in the resulting table		
		var rows =tabledata.all(by.tagName("tr"));		//Get the rows		
		var cells = rows.all(by.tagName("td"));		

		expect(cells.get(0).getText()).toEqual("project4");	
		expect(cells.get(1).getText()).toEqual("python, angular4, MongoDb");
		expect(cells.get(2).getText()).toEqual("KANBAN");
	})	
});

describe("try searching a project",function(){
	it("to test the prokect option search by methodology", function(){
		browser.get('http://localhost:4000/search-basic');
		element(by.css('.custom-control.custom-radio.metod')).click();
		element(by.css('input[type="text"]')).clear().sendKeys('KANBAN');;
		browser.actions().sendKeys(protractor.Key.ENTER).perform();
		
		var tabledata =element.all(by.css("table"));	//Get the values in the resulting table		
		var rows =tabledata.all(by.tagName("tr"));		//Get the rows		
		var cells = rows.all(by.tagName("td"));		

		expect(cells.get(0).getText()).toEqual("project4");	
		expect(cells.get(1).getText()).toEqual("python, angular4, MongoDb");
		expect(cells.get(2).getText()).toEqual("KANBAN");
	})	
});

describe("try searching a project",function(){
	it("to test the behavior when a project is searched in an incorrect option with a proper value", function(){
		browser.get('http://localhost:4000/search-basic');
		element(by.css('.custom-control.custom-radio.metod')).click();
		element(by.css('input[type="text"]')).clear().sendKeys('python');;
		browser.actions().sendKeys(protractor.Key.ENTER).perform();		
		
		expect(element(by.css("table")).isPresent()).toBe(false);	
	})	
});

