package com.employeeDashboardFunctionalTests.test;

import com.employeeDashboardFunctionalTests.common.Search;



import io.github.bonigarcia.wdm.ChromeDriverManager;
import io.github.bonigarcia.wdm.FirefoxDriverManager;
import java.util.Objects;
import org.junit.After;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;



//HomePage homePage = new HomePage(driver);
        

       // homePage.loadHomePage();
        //homePage.search("dress");

        

public class EP160Test {

    

	private WebDriver driver;
        
       

	@BeforeClass
	public static void setupClass() {
		ChromeDriverManager.getInstance().setup();
	}

    

	@Before
	public void setupTest() {
		driver = new ChromeDriver();
	}

	@After
	public void teardown() {
		if (driver != null) {
			driver.quit();
		}
	}
        
        /*
        Verify the user’s name is shown when he is at the skills visualization
        */
	@Test
	public void userNameVerificationtest() {
           
            driver.get("http://localhost:4000/skills/59fa43c4b20c0dc4d7d8bc79");
            Search searchResultPage = new Search(driver) {};
            boolean isNameDisplayed = searchResultPage.isResultItemDisplayed("Juan Pérez");
            assertTrue("aplica",isNameDisplayed);
            
	}
        
        /*
        Verify that the hard skills details are shown when you click in a hard skill
        */
        @Test
	public void hardSkillClickRedirectiontest() throws InterruptedException {
           
            driver.get("http://localhost:4000/skills/59fa43c4b20c0dc4d7d8bc79");
            
            
            WebElement hardSkill = driver.findElement(By.name("Spring"));
            Thread.sleep(1000);
            hardSkill.click();
            Thread.sleep(1000);
            driver.findElement(By.xpath(".//h3[text() = 'DETALLES DE LA VALORACIÓN']"));
            Thread.sleep(1000);

	}
        
       

}


