package com.employeeDashboardFunctionalTests.test;
import com.employeeDashboardFunctionalTests.common.Search;

import io.github.bonigarcia.wdm.ChromeDriverManager;
import io.github.bonigarcia.wdm.FirefoxDriverManager;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import org.junit.After;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.Every.everyItem;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.collection.IsEmptyCollection.*;



public class EP154Test {

    

	private WebDriver driver;

	@BeforeClass
	public static void setupClass() {
		ChromeDriverManager.getInstance().setup();
	}

	@Before
	public void setupTest() {
		driver = new ChromeDriver();
	}

	@After
	public void teardown() {
		if (driver != null) {
			driver.quit();
		}
	}
        
     
        
        /*
        Verify the user’s name is shown when he is at the projects visualization
        */
	@Test
	public void userNameIsShownTest() {
           
            driver.get("http://localhost:4000/");
            Search searchResultPage = new Search(driver);
            boolean isNameDisplayed = searchResultPage.isResultItemDisplayed("Juan Pérez");
            assertThat(isNameDisplayed, is(true));
            
	}
        
        
       /*
        Verify that the view of the skills(soft and hard) is shown when you click in  a project
        */
        @Test
	public void navigationFromProjectTest() {
           
            driver.get("http://localhost:4000/");
            
            
            WebElement project = driver.findElement(By.name("Project1"));
                    
            project.click();
            
            String skillsPageTitle = driver.getTitle();
            assertThat(skillsPageTitle, is("Skills"));
            
            

	}
        
        /*
        Verify that when the current employee has no projects, there should be a message saying "El usuario no ha participado en ningun proyecto"
        */
        @Test
	public void ListOfProjectsNullTest() {
           
            driver.get("http://localhost:4000/");
                
            List<WebElement> projects = driver.findElements(By.id("Project"));
   
            if(projects.isEmpty()){
                WebElement Message =driver.findElement(By.id("message"));
                String message = Message.getText();
                assertThat(message, is("El usuario no ha participado en ningun proyecto"));
            }else{
                assertThat(projects, is(not(empty())));
            }
            
            
            
     
            
	}
        
        
       

}

