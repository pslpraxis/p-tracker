package com.employeeDashboardFunctionalTests.test;
import com.employeeDashboardFunctionalTests.common.Search;

import io.github.bonigarcia.wdm.ChromeDriverManager;
import io.github.bonigarcia.wdm.FirefoxDriverManager;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import org.junit.After;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.Every.everyItem;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.lessThan;
import static org.hamcrest.Matchers.lessThanOrEqualTo;
import static org.hamcrest.collection.IsEmptyCollection.*;



public class EP157Test {

    

	private WebDriver driver;
        
       

	@BeforeClass
	public static void setupClass() {
		ChromeDriverManager.getInstance().setup();
	}

    

	@Before
	public void setupTest() {
		driver = new ChromeDriver();
	}

	@After
	public void teardown() {
		if (driver != null) {
			driver.quit();
		}
	}
       
        
         /*
        Verify the user’s name is shown when he is at the projects visualization
        */
	@Test
	public void userNameIsShownTest() {
           
            driver.get("http://localhost:4000/skills/5a063839dbc58f8b1c2602a1");
            Search searchResultPage = new Search(driver);
            boolean isNameDisplayed = searchResultPage.isResultItemDisplayed("Juan Pérez");
            assertThat(isNameDisplayed, is(true));
            
	}
        
         /*
        Verify that the soft skills details are shown when you click in a hard skill
        */
        
         @Test
	public void navigationFromSoftSkillTest() {
           
            driver.get("http://localhost:4000/skills/5a063839dbc58f8b1c2602a1");
            
            
            WebElement project = driver.findElement(By.name("Java"));
                    
            project.click();
            
            String skillsPageTitle = driver.getTitle();
            assertThat(skillsPageTitle, is("Valoration details"));
            
            

	}
        
        
          /*
        Verify that the varloration of a specific soft skill is greater or equal than 0 and less or equal than 5
        */
        @Test
	public void softSkillValorationTest(){
           
            driver.get("http://localhost:4000/skills/5a063839dbc58f8b1c2602a1");
            
            List<WebElement> valorations = driver.findElements(By.id("ValorationSoftSkill"));
           
            
            for (WebElement valoration : valorations) {
                String val=valoration.getText();
                
             
                assertThat(Integer.parseInt(val),greaterThanOrEqualTo(0));
                assertThat(Integer.parseInt(val),lessThanOrEqualTo(5));
            }
            

	}
       
        
       
        
       

}


