/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.employeeDashboardFunctionalTests.common;

import java.util.Objects;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 *
 * @author JUAN PABLO ABAD
 */
public class Search {
    private final WebDriver driver;

    public Search(final WebDriver driver) {
        Objects.requireNonNull(driver);
        this.driver = driver;
    }

    public boolean isResultItemDisplayed(String itemName) {
        return driver.findElement(By.name(itemName)).isDisplayed();
    }
    
    public boolean isResultItemDisplayedById(String id) {
        return driver.findElement(By.id(id)).isDisplayed();
    }
   
}
