process.env.NODE_ENV = 'test';

const mongoose = require("mongoose");
const Hardskill = require('../models/hardskill');

const agent = require('superagent-promise')(require('superagent'), Promise);
const statusCode = require('http-status-codes');
const chai = require('chai');
const chaiHttp = require('chai-http');
const _ = require('lodash');
const app = require('../app');

const expect = chai.expect;
const should = chai.should();
const HardskillController = require('../controllers/hardskills');

chai.use(chaiHttp);

describe('Hardskills', () =>{

  beforeEach((done) => {
    Hardskill.remove({}, (err) => {
      done();
    });
  });

  describe('GET /hardskill', () =>{
    it('it should GET all the hardskill', (done) =>{
      chai.request(app)
        .get('/hardskills')
        .end((err, res) =>{
          res.should.have.status(200);
          res.body.should.be.a('array');
          res.body.should.have.length(0);
          done();
        });
    });
  });

  describe('GET /hardskill/:hardskillId', () => {
		it('it should GET a hardskill by the given Id', (done) => {
			const hardskill = new Hardskill({name: "Test hardskill"});
			hardskill.save((err, hardskill) => {
				chai.request(app)
				.get('/hardskills/' + hardskill._id)
				.send(hardskill)
				.end((err, res) => {
					res.should.have.status(200);
					res.body.should.have.property('name');
					res.body.should.have.property('_id');
					res.body.should.be.a('object');
					done();
				});
			});
		});

		it('it should GET a joi validation with a wrong Id', (done) => {
			const wrongId = "NO_AN_ACTUAL_ID";

			const hardskill = new Hardskill({name: "Test hardskill"});

			hardskill.save((err, hardskill) => {
				chai.request(app)
				.get('/hardskills/' + wrongId)
				.send(hardskill)
				.end((err, res) => {
					// Error status send by joi module
					res.should.have.status(400);

					// Validation from Helper should be here
					res.body.should.have.property('isJoi');
					res.body.should.be.a('object');
					done();
				});
			});
		});

		it('it should GET a null with a correct but non existent Id', (done) => {
			const wrongId = "000000000000000000000000";

			const hardskill = new Hardskill({name: "Test hardskill"});

			hardskill.save((err, hardskill) => {
				chai.request(app)
				.get('/hardskills/' + wrongId)
				.send(hardskill)
				.end((err, res) => {
					res.should.have.status(200);
					should.equal(res.body, null);
					done();
				});
			});
		});

	});

  describe('/POST hardskill', () => {
		it('it should not POST empty hardskill', (done) => {
			const hardskill = {};

			chai.request(app)
				.post('/hardskills')
				.send(hardskill)
				.end((err, res) => {
					// Error status send by joi module
					res.should.have.status(400);

					// Validation from Helper should be here
					res.body.should.have.property('isJoi');
					res.body.should.be.a('object');
					done();
				});
		});

		it('it should POST hardskill', (done) => {
			const hardskill = {
				name: "Test of hardskill"
			};

			chai.request(app)
				.post('/hardskills')
				.send(hardskill)
				.end((err, res) => {
					// Error status send by joi module
					res.should.have.status(201);

					// Validation from Helper should be here
					res.body.should.have.property('name');
					res.body.should.have.property('_id');
					res.body.should.be.a('object');
					done();
				});
		});

	});













})
