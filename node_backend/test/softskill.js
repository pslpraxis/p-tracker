process.env.NODE_ENV = 'test';

const mongoose = require("mongoose");
const Softskill = require('../models/softskill');

const agent = require('superagent-promise')(require('superagent'), Promise);
const statusCode = require('http-status-codes');
const chai = require('chai');
const chaiHttp = require('chai-http');
const _ = require('lodash');
const app = require('../app');

const expect = chai.expect;
const should = chai.should();
const SoftskillController = require('../controllers/softskills');

chai.use(chaiHttp);

describe('Softskills', () =>{

  beforeEach((done) => {
    Softskill.remove({}, (err) =>{
      done();
    });
  });

  describe('GET /softskill', () =>{
    it('it should GET all softskill ', (done) =>{
      chai.request(app)
      .get('/softskills')
      .end((err, res) =>{
         res.should.have.status(200);
         res.body.should.be.a('array');
         res.body.should.have.length(0);
         done();
       });
    });
  });

  describe('GET /softskill/:softskillId', () =>{

    it('it should GET a Softskill by the given ID', (done) =>{
      const softskill = new Softskill({name: "etica"});
      softskill.save((err, softskill) =>{
        chai.request(app)
        .get('/softskills/' + softskill._id)
        .send(softskill)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.have.property('_id');
          res.body.should.have.property('name');
          res.body.should.be.a('object');
          done();
        });
      });
    });

    it('it should GET a joi validation with a wrong ID', (done) => {
      const wrongId = "NO_AN_ACTUAL_ID";
      const softskill = new Softskill({name: "etica"});
      softskill.save((err, softskill) =>{
        chai.request(app)
        .get('/softskills/' + wrongId)
        .send(softskill)
        .end((err, res) => {

          res.should.have.status(400);
          res.body.should.have.property('isJoi');
          res.body.should.be.a('object');
          done();
        });
      });
    });

    it('it should GET a null with a correct but non existent Id', (done) => {
      const wrongId = "000000000000000000000000";
      const softskill = new Softskill({name: "etica"});
      softskill.save((err, softskill) => {
        chai.request(app)
        .get('/softskills/' + wrongId)
        .send(softskill)
        .end((err, res) => {
          res.should.have.status(200);
          should.equal(res.body, null);
          done();
        });
      });
    });

  });

  describe('/POST softskill', () =>{
    it('it should not POST empty softskill', (done) =>{
      const softskill = {};

      chai.request(app)
       .post('/softskills')
       .send(softskill)
       .end((err, res) =>{

         res.should.have.status(400);
         res.body.should.have.property('isJoi');
         res.body.should.be.a('object');
         done();
       });
    });

    it('it should POST softskill', (done) =>{
      const softskill = {
                          name: "etica"
                        };

      chai.request(app)
       .post('/softskills')
       .send(softskill)
       .end((err, res) => {

         res.should.have.status(201);
         res.body.should.have.property('name');
         res.body.should.have.property('_id');
         res.body.should.be.a('object');
         done();
       });
    });
  });




})
