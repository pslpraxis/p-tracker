process.env.NODE_ENV = 'test';

const mongoose = require("mongoose");
const Employee = require('../models/employee');

const agent = require('superagent-promise')(require('superagent'), Promise);
const statusCode = require('http-status-codes');
const chai = require('chai');
const chaiHttp = require('chai-http');
const _ = require('lodash');
const app = require('../app');

const expect = chai.expect;
const should = chai.should();
const EmployeeController = require('../controllers/employees');

chai.use(chaiHttp);

describe('Employees', () => {

  beforeEach((done) => {
    Employee.remove({}, (err) =>{
      done();
    });
  });

 describe('GET /employee', () =>{
   it('it should GET all employees', (done) =>{
     chai.request(app)
     .get('/employees')
     .end((err, res) =>{
        res.should.have.status(200);
        res.body.should.be.a('array');
        res.body.should.have.length(0);
        done();
      });
   });
 });

 describe('GET /employee/:employeeId', () =>{

   it('it should GET a Employee by the given ID', (done) =>{
     const employee = new Employee({name: "Test Employee"});
     employee.save((err, employee) =>{
       chai.request(app)
       .get('/employees/' + employee._id)
       .send(employee)
       .end((err, res) => {
         res.should.have.status(200);
         res.body.should.have.property('_id');
         res.body.should.have.property('name');
         //res.body.should.have.property('softskills');
         //res.body.should.have.property('hardskills');
         //res.body.should.have.property('projects');
         res.body.should.be.a('object');
         done();
       });
     });
   });

   it('it should GET a joi validation with a wrong ID', (done) => {
     const wrongId = "NO_AN_ACTUAL_ID";
     const employee = new Employee({name: "Test Employee"});
     employee.save((err, employee) =>{
       chai.request(app)
       .get('/employees/' + wrongId)
       .send(employee)
       .end((err, res) => {

         res.should.have.status(400);
         res.body.should.have.property('isJoi');
         res.body.should.be.a('object');
         done();
       });
     });
   });

   it('it should GET a null with a correct but non existent Id', (done) => {
     const wrongId = "000000000000000000000000";
     const employee = new Employee({name: "Test employee"});
     employee.save((err, employee) => {
       chai.request(app)
       .get('/employees/' + wrongId)
       .send(employee)
       .end((err, res) => {
         res.should.have.status(200);
         should.equal(res.body, null);
         done();
       });
     });
   });

 });

 describe('/POST employee', () =>{
   it('it should not POST empty employee', (done) =>{
     const employee = {};

     chai.request(app)
      .post('/employees')
      .send(employee)
      .end((err, res) =>{

        res.should.have.status(400);
        res.body.should.have.property('isJoi');
        res.body.should.be.a('object');
        done();
      });
   });

   it('it should POST employee', (done) =>{
     const employee = {
                         name: "Test"
                       };

     chai.request(app)
      .post('/employees')
      .send(employee)
      .end((err, res) => {

        res.should.have.status(201);
        res.body.should.have.property('name');
        res.body.should.have.property('_id');
        res.body.should.be.a('object');
        done();
      });
   });
 });



});
