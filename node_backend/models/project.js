const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const projectSchema = new Schema({

	name: String,
	projects_associated: [{
		type: Schema.Types.ObjectId,
		ref: 'project'
	}],
	vertical_product: String,
	other_vertical_product: String,
	//IBU_ECU: String,
	SO_client: String,
	natl_intl: String,
	language: String,
	languages_and_frameworks: [String],
	tools_and_libraries: [String],
	plataforms_and_tools: [String],
	techniques_and_practices: [String],
	JIRA_PSL: Boolean,
	project_management_tool: String,
	repository_tool: String,

	/*
	employees: [{
		type: Schema.Types.ObjectId,
		ref: 'employee'
	}]

	hard_skills: [{
		type: Schema.Types.ObjectId,
		ref: 'hardskill'
	}]

	soft_skills: [{
		type: Schema.Types.ObjectId,
		ref: 'softskill'
	}]

	*/

});

const Project = mongoose.model('project', projectSchema);

module.exports = Project;
