const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const EmployeeSchema = Schema ({

  name: String,
  cellphone: Number,
  email: String,
  workPlace : String,
//  EmploymentContract: Number,
  projects: [{
  	type: Schema.Types.ObjectId,
  	ref: 'project'
  }],
  userAccount: String,
  emailPSL: String,
  skypeAccount: String,
  userPermission :String,
  role: {
    type:String,
    enum:["Backend dev",
          "Scrum Master",
          "Frontend Dev",
          "Tester",
          "Configuration manager",
          "Architect"]},
  hardskills: [{
    type: Schema.Types.ObjectId,
    ref: 'hardskill'
  }],
  softskills: [{
    type: Schema.Types.ObjectId,
    ref: 'softskill'
  }]
  
})

const Employee = mongoose.model('employee', EmployeeSchema);

module.exports = Employee;
