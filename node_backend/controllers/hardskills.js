// Importing Hardskills model to manage hardskills database
const Hardskill = require('../models/hardskill');

module.exports = {

	index: (req, res, next) => {
		Hardskill.find({})
		.then(hardskills => {
			res.status(200).json(hardskills);
		})
		.catch(err => {
			next(err);
		})
	},

	newHardskill: (req, res, next) => {
		const newHardskill = new Hardskill(req.value.body);
		newHardskill.save()
			.then(hardskill => {
				res.status(201).json(newHardskill);
			})
			.catch(err => {
				next(err);
			})
	},

	getHardskill: (req, res, next) => {
		const hardskillId = req.params.hardskillId;

		// Fetch hardskill by id
		Hardskill.findById(hardskillId)
			.then(hardskill => {
				// If hardskill was found send it with a 200 status
				res.status(200).json(hardskill);
			})
			.catch(err => {
				// If there is an error pass control
				next(err);
			})
	},

	replaceHardskill: (req, res, next) => {
		// Get the object id whos going to get replace
		const hardskillId = req.params.hardskillId;

		//Get json with the new object attributes
		const newHardskill = req.body;

		// Look for the object whos gonna be replaced
		Hardskill.findByIdAndUpdate(hardskillId, newHardskill)
			.then(result => {
				// If changed the object send a success message
				res.status(200).json({success:true});
			})
			.catch(err => {
				// Send error
				next(err);
			})

	},

	updateProject: (req, res, next) => {
		// Get object id to replace
		const hardskillId = req.params.hardskillId;
		// Get fields to change
		const newHardskill = req.body;

		// Find in database and update
		Hardskill.findbyIdAndUpdate(hardskillId, newHardskill)
			.then(result => {
				// Send 200 status and a success if everything ok
				res.status(200).json({success:true});
			})
			.catch(err => {
				// If there was an error
				next(err);
			})


	}

};
