const Softskill = require('../models/softskill');

module.exports = {

	index: (req, res, next) => {
		Softskill.find({})
			.then(softskills => {
				res.status(200).json(softskills);
			})
			.catch(err => {
				next(err);
			})
	},

	newSoftskill: (req, res, next) => {
		const newSoftskill = new Softskill(req.body);
		newSoftskill.save()
			.then(softskill => {
				res.status(201).json(softskill);
			})
			.catch(err => {
				next(err);
			})
	},

	getSoftskill: (req, res, next) => {
		const softskillId = req.params.softskillId;

		Softskill.findById(softskillId)
			.then(softskill => {
				res.status(200).json(softskill);
			})
			.catch(err => {
				next(err);
			})
	},

	replaceSoftskill: (req, res, next) => {
		const softskillId = req.params.softskillId;

		const newSoftskill = req.body;

		Softskill.findByIdAndUpdate(softskillId, newSoftskill)
			.then(result => {
				res.status(200).json({success:true});
			})
			.catch(err => {
				next(err);
			})
	},

	updateSoftskill: (req, res, next) => {
		const softskillId = req.params.softskillId;

		const newSoftskill = req.body;

		Softskill.findByIdAndUpdate(softskillId, newSoftskill)
			.then(result => {
				res.status(200).json({success:true});
			})
			.catch(err => {
				next(err);
			})
	},

	deleteSoftskill: (req, res, next) => {
		const softskillId = req.params.softskillId;

		Softskill.remove({_id:softskillId})
			.then(result => {
				res.status(200).json({success:true});
			})
			.catch(err => {
				next(err);
			})
	}



}
