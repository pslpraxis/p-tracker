const express = require('express');
const router = express.Router();

const SoftskillController = require('../controllers/softskills');

const {validateParam, validateBody, schemas} = require('../helpers/softskill');

router.route('/')
	.get(SoftskillController.index)
	.post(validateBody(schemas.softskillSchema),SoftskillController.newSoftskill);

router.route('/:softskillId')
	.get(validateParam(schemas.idSchema, 'softskillId'),SoftskillController.getSoftskill)
	.put([validateParam(schemas.idSchema, 'softskillId'), validateBody(schemas.softskillSchema)],
				SoftskillController.replaceSoftskill)
	.patch([validateParam(schemas.idSchema, 'softskillId'), validateBody(schemas.softskillOptionalSchema)],
				SoftskillController.updateSoftskill)
	.delete(validateParam(schemas.idSchema, 'softskillId'),SoftskillController.deleteSoftskill);


// Make public the router object with the softskills resources.
module.exports = router;
