const express = require('express');
const router = express.Router();

const EmployeeController = require('../controllers/employees')

const {validateParam, validateBody, schemas} = require('../helpers/employee');

router.route('/')

  .get(EmployeeController.index)
  .post(validateBody(schemas.EmployeeSchema),EmployeeController.saveEmployee);

router.route('/:employeeId')
	.get(validateParam(schemas.idSchema, 'employeeId'),EmployeeController.getEmployeeId)
	.put([validateParam(schemas.idSchema, 'employeeId'),validateBody(schemas.EmployeeSchema)],
       EmployeeController.replaceEmployee)
	.patch([validateParam(schemas.idSchema, 'employeeId'),validateBody(schemas.EmployeeOptionalSchema)],
      EmployeeController.updateEmployee)
	.delete(validateParam(schemas.idSchema, 'employeeId'),EmployeeController.deleteEmployee);

router.route('/:employeeId/projects/')
	.get(validateParam(schemas.idSchema, 'employeeId'),EmployeeController.getProjects)
	.post(validateParam(schemas.idSchema, 'employeeId'),EmployeeController.newProjects);

router.route('/:employeeId/hardskills/')
	.get(validateParam(schemas.idSchema, 'employeeId'),EmployeeController.getHardskills)
	.post(validateParam(schemas.idSchema, 'employeeId'),EmployeeController.newHardskills);

router.route('/:employeeId/softskills/')
	.get(validateParam(schemas.idSchema, 'employeeId'),EmployeeController.getSoftskills)
	.post(validateParam(schemas.idSchema, 'employeeId'),EmployeeController.newSoftskills);

router.route('/:employeeId/:projectId/hardskill')
	.get(EmployeeController.getHardEvaluation)
	.post(EmployeeController.newHardEvaluation);

router.route('/:employeeId/:projectiD/softskill')
	.get(EmployeeController.getSoftEvaluation)
	.post(EmployeeController.newSoftEvaluation);

module.exports = router;
