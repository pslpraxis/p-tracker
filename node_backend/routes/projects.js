const express = require('express');
const router = express.Router();

// Importing the Projects Controller
const ProjectsController = require('../controllers/projects');

// Importing validatoin middleware
const {validateParam, validateBody, schemas} = require('../helpers/project');

// Handling root call for projects resource
router.route('/')
	.get(ProjectsController.index) // List all projects
	.post(validateBody(schemas.projectSchema), ProjectsController.newProject);

// Handling when there is a param in the url
router.route('/:projectId')
	.get(validateParam(schemas.idSchema, 'projectId'),ProjectsController.getProject)
	.put([validateParam(schemas.idSchema, 'projectId'),validateBody(schemas.projectSchema)],
				ProjectsController.replaceProject)
	.patch([validateParam(schemas.idSchema, 'projectId'),validateBody(schemas.projectOptionalSchema)],
				ProjectsController.updateProject);

// Handling the project relationship with other projects
router.route('/:projectId/projects')
	.get(validateParam(schemas.idSchema, 'projectId'),ProjectsController.getProjectAssociatedProjects)
	.post(validateParam(schemas.idSchema, 'projectId'),ProjectsController.newProjectAssociatedProjects);

// Make public the router object with the project resources.
module.exports = router;
