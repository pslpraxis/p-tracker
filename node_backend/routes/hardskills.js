const express = require('express');
const router = express.Router();

const HardskillsController = require('../controllers/hardskills');

const {validateParam, validateBody, schemas} = require('../helpers/hardskill');

router.route('/')
	.get(HardskillsController.index)
	.post(validateBody(schemas.hardskillSchema),HardskillsController.newHardskill);

router.route('/:hardskillId')
	.get(validateParam(schemas.idSchema, 'hardskillId'),HardskillsController.getHardskill)
	.put([validateParam(schemas.idSchema, 'hardskillId'), validateBody(schemas.hardskillSchema)],
			HardskillsController.replaceHardskill);

// Make public the router object with the hardskills resources.
module.exports = router;
