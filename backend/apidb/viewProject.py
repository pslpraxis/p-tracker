from django.http import JsonResponse

from . import mongoProject


def index(request):
    return JsonResponse({'Root': 'Api'})

def projects(request):
	return JsonResponse(mongoProject.getProjects(), safe=False)

def projectId(request, id):
	return JsonResponse(mongoProject.getProjectId(id))

def projectName(request, name):
	return JsonResponse(mongoProject.getProjectName(name), safe=False)

