from pymongo import MongoClient
from bson import json_util
import json
from bson.json_util import dumps


import pprint

#Connection to the Mongo Server
mongoClient = MongoClient('localhost',27017)

#Accessing the database
db = mongoClient.ptracker

#Get a collection
collection = db.projects

def getProjects():
	cursor1 = collection.find()
	page_sanitized = json.loads(dumps(cursor1))
	return page_sanitized

def getProjectId(id):
	id = int(id)
	for cursor2 in collection.find({"project_id": id}):
		data = dumps(cursor2)

	page_sanitized = json.loads(data)
	return page_sanitized

def getProjectName(name):
	name = str(name)
	cursor=collection.find({'$or':[{'project_name': {'$regex':name}},{'methodology': {'$regex':name}}, {'languagesFrameworks':{'$regex':name}} ]},{'project_id':1,'project_name':1,'languagesFrameworks':1,'toolsAndLibraries':1,'plataformsTools':1,'methodology':1});
	page_sanitized = json.loads(dumps(cursor))
	return page_sanitized

